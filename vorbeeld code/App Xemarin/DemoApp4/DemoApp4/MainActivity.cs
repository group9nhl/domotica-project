﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Timers;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

// Dick Bruin, 13/12/2015
// Demo of timers and sockets
// in combination with chat.ino on an Arduino

namespace DemoApp4
{
    [Activity(Label = "DemoApp4", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Timer timer;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button buttonSend = FindViewById<Button>(Resource.Id.buttonSend);
            CheckBox checkBoxForce = FindViewById<CheckBox>(Resource.Id.checkBoxForce);
            EditText editTextIP = FindViewById<EditText>(Resource.Id.editTextIP);
            EditText editTextSend = FindViewById<EditText>(Resource.Id.editTextSend);
            TextView textViewMessage = FindViewById<TextView>(Resource.Id.textViewMessage);

            checkBoxForce.Checked = bundle != null ? bundle.GetBoolean("timerState") : false; // restore timerState

            timer = new Timer() { Interval = 1000, Enabled = checkBoxForce.Checked };
            timer.Elapsed += (obj, args) => { RunOnUiThread(() => { textViewMessage.Text = ask(editTextIP.Text, 50007, "force"); } ); };

            editTextIP.TextSize = 25;
            editTextSend.TextSize = 25;
            textViewMessage.TextSize = 25;

            buttonSend.Click += (obj, args) => { textViewMessage.Text = ask(editTextIP.Text, 50007, editTextSend.Text); };
            checkBoxForce.Click += (obj, args) => { timer.Enabled = checkBoxForce.Checked; };
        }

        // save timer.Enabled when the phone changes direction
        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);

            outState.PutBoolean("timerState", timer.Enabled);
            timer.Dispose();
        }

        // simple socket functions
        public Socket open(string ipaddress, int portnr)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ip = IPAddress.Parse(ipaddress);
            IPEndPoint endpoint = new IPEndPoint(ip, portnr);
            socket.Connect(endpoint);
            return socket;
        }

        public void write(Socket socket, string text)
        {
            socket.Send(Encoding.ASCII.GetBytes(text));
        }

        public string read(Socket socket)
        {
            byte[] bytes = new byte[4096];
            int bytesRec = socket.Receive(bytes);
            string text = Encoding.ASCII.GetString(bytes, 0, bytesRec);
            return text;
        }

        public void close(Socket socket)
        {
            socket.Close();
        }

        // datagram like conversation with server
        public string ask(string ipaddress, int portnr, string message)
        {
            Socket s = open(ipaddress, portnr);
            write(s, message);
            string reply = read(s);
            close(s);
            return reply;
        }
    }
}

