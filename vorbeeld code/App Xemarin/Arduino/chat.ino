#include <SPI.h>                  // Ethernet shield uses SPI-interface
#include <Ethernet.h>             // Ethernet library

byte mac[] = { 0x40, 0x6c, 0x8f, 0x36, 0x84, 0x8a }; // Ethernet adapter shield S. Oosterhaven
EthernetServer server(50007);                        // EthernetServer instance (listening on port <ethPort>)
bool connected = false;

void setup() 
{
   Serial.begin(9600);
   
   if(Ethernet.begin(mac) == 0) return;
   Serial.print("Listening on address: ");
   Serial.println(Ethernet.localIP());
   server.begin();
   connected = true;
}

void loop() 
{
   if(!connected) return;
   EthernetClient ethernetClient = server.available();
   if(!ethernetClient) return;
   
   Serial.println("Application connected");
   while (ethernetClient.connected()) 
   {
     char buffer[128];
     int count = 0;
     while (ethernetClient.available()) 
     {
        buffer[count++] = ethernetClient.read();
     }
     buffer[count] = '\0';
     
     if(count > 0 )
     {
       Serial.println(buffer);
       if(String(buffer) == String("force"))
       {
         int force = analogRead(0);
         ethernetClient.print(force);
         Serial.println(force);
       }
       else
         ethernetClient.print(buffer);
     }
   }
   Serial.println("Application disconnected");
}
