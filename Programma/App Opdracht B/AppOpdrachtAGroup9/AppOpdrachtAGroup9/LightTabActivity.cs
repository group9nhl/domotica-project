using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Net;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "LightTabActivity")]
    public class LightTabActivity : Activity
    {
        private Button lightButton1, lightButtonSendSensorValue, lightButtonSendTimePickerValue;

        private EditText lightSensorValueEditText;

        public static TextView lightKakStatus1, lightSensorDisplayTextView;

        private TimePicker lightTimePicker;

        private List<Button> buttons = new List<Button>();



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Light);

            var enc = Encoding.ASCII;

            Connection.main.LightTab = this;

            lightButton1 = FindViewById<Button>(Resource.Id.lightTabButton1);
            lightButtonSendSensorValue = FindViewById<Button>(Resource.Id.submitLightSensorValue);
            lightButtonSendTimePickerValue = FindViewById<Button>(Resource.Id.sumbitLightTimePicker);

            lightSensorValueEditText = FindViewById<EditText>(Resource.Id.editTextSensorLightValue);

            lightSensorDisplayTextView = FindViewById<TextView>(Resource.Id.lightSensorValueTextViewDisplay);

            lightKakStatus1 = FindViewById<TextView>(Resource.Id.lightKakStatus1);

            lightTimePicker = FindViewById<TimePicker>(Resource.Id.lightTimePicker);

            #region addbuttons
            buttons.Add(lightButton1);
            buttons.Add(lightButtonSendSensorValue);
            buttons.Add(lightButtonSendTimePickerValue);
            #endregion

            foreach (Button button in buttons)
            {
                Util.setTextColorAndShadow(button, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            }
            Util.setTextColorAndShadow(lightKakStatus1, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            Util.setTextColorAndShadow(lightSensorValueEditText, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);

            //light switch button
            if (lightButton1 != null)
            {
                lightButton1.Click += (sender, e) =>
                {
                    lightButton1.Text = "Setting t(1)..";
                    Connection.main.socket.Send(enc.GetBytes("t"));
                };
            }

            if (lightButtonSendSensorValue != null)
            {
                lightButtonSendSensorValue.Click += (sender, e) =>
                {
                    Util.setTextComponent(lightButtonSendSensorValue, "Sending 'u'");

                    Connection.main.socket.Send(enc.GetBytes("u"));
                    Connection.main.socket.Send(enc.GetBytes(Util.getLengthAsCharArray(lightSensorValueEditText)));
                    Connection.main.socket.Send(enc.GetBytes(Util.getValueInCharArray(lightSensorValueEditText)));
                };
            }

            if (lightButtonSendTimePickerValue != null)
            {
                lightButtonSendTimePickerValue.Click += (sender, e) =>
                {
                    char[] timeDifferenceInCharArray =
                        Util.GetTimeDifferenceFrom(Util.GetCurrentTimeInSeconds(lightTimePicker.Hour,
                            lightTimePicker.Minute)).ToString().ToCharArray();

                    char[] countString = timeDifferenceInCharArray.Length.ToString().ToCharArray();

                   Connection.main.socket.Send(enc.GetBytes("v"));
                   Connection.main.socket.Send(enc.GetBytes(countString));
                    Connection.main.socket.Send(enc.GetBytes(timeDifferenceInCharArray));
                };
            }
        }
        public void ChangeText(string result, Color color)
        {
            Debug.WriteLine(lightKakStatus1);
            lightKakStatus1.Text = result;
            Util.setTextColor(lightKakStatus1, color);
        }

        public void changeLightSensorValueText(string result, Color color)
        {
            Debug.WriteLine(lightSensorDisplayTextView);
            lightSensorDisplayTextView.Text = result;
            Util.setTextColor(lightSensorDisplayTextView, color);
        }
    }
}