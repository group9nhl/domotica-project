using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "SecurityTabActivity")]
    public class SecurityTabActivity : Activity
    {
        public static Button buttonONOFFsecurity;
        private Button Securitybutton2;

        public TextView Securitytextview1,
            TextViewUselessSecurity,
            TextViewSensor1,
            TextViewSensor2,
            TextViewAlarmStatus,
            textViewFireStatus;

        List<Button> SecurityButtonlist = new List<Button>();
        List<TextView> SecurityTextViewList = new List<TextView>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Security);

            Connection.main.SecurityTab = this;

            buttonONOFFsecurity = FindViewById<Button>(Resource.Id.button1Security);
            TextViewUselessSecurity = FindViewById<TextView>(Resource.Id.textView2Security);
            TextViewSensor1 = FindViewById<TextView>(Resource.Id.textView3Security);
            TextViewSensor2 = FindViewById<TextView>(Resource.Id.textView4Security);
            TextViewAlarmStatus = FindViewById<TextView>(Resource.Id.textView5Security);
            textViewFireStatus = FindViewById<TextView>(Resource.Id.textViewFireAlarmstatus);

            SecurityButtonlist.Add(buttonONOFFsecurity);

            SecurityTextViewList.Add(TextViewUselessSecurity);
            SecurityTextViewList.Add(TextViewSensor1);
            SecurityTextViewList.Add(TextViewSensor2);
            SecurityTextViewList.Add(TextViewAlarmStatus);
            SecurityTextViewList.Add(textViewFireStatus);

            foreach (Button securitybutton in SecurityButtonlist)
            {
                Util.setTextColorAndShadow(securitybutton, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            }
            foreach (TextView textview in SecurityTextViewList)
            {
                Util.setTextColorAndShadow(textview, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            }

            if (buttonONOFFsecurity != null)
            {
                buttonONOFFsecurity.Click += (sender, e) =>
                {
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("e"));
                };
            }


        }


        public void ChangeText(string result, Color color, int id)
        {
            switch (id)
            {

                case 0: //for ultrasone 1

                    Debug.WriteLine(TextViewSensor1);
                    TextViewSensor1.Text = result;
                    Util.setTextColor(TextViewSensor1, color);
                    break;
                case 1: //for ultrasone 2

                    Debug.WriteLine(TextViewSensor2);
                    TextViewSensor2.Text = result;
                    Util.setTextColor(TextViewSensor2, color);
                    break;
                //for kak status
                case 2:

                    Debug.WriteLine(TextViewAlarmStatus);
                    buttonONOFFsecurity.Text = result;
                    Util.setTextColor(buttonONOFFsecurity, color);
                    break;
            }
        }

        public void ChangeSecurityText(string result, Color color)
        {
            Debug.WriteLine(TextViewAlarmStatus);
            TextViewAlarmStatus.Text = result;
            Util.setTextColor(TextViewAlarmStatus, color);
        }

        public void ChangeFireSensorText(string result, Color color)
        {
            Debug.WriteLine(result);
            Debug.WriteLine(color);
            textViewFireStatus.Text = result;
            Util.setTextColor(textViewFireStatus, color);
        }

        public void PushNotification(string title, string message)
        {
            Debug.WriteLine("ATTEMPTING TO SEND NOTIFICATION {0}:{1}", title, message);
            // Instantiate the builder and set notification elements:
            Notification.Builder
                builder = new Notification.Builder(this)
                    .SetContentTitle(title)
                    .SetContentText(message)
                    .SetSmallIcon(Resource.Drawable.ic_tab_security_selected);

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager =
                GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);
        }
    }
}