using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Icu.Text;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppOpdrachtAGroup9
{
    /// <summary>
    /// Has some standard settings for the app so the style is always the same
    /// @author Jan Julius
    /// </summary>
    public class Settings
    {
        private static MainActivity mainactivity;


        public static class textInfo
        {
            //standard text color
            public static Color STANDARD_COLOR = Color.AntiqueWhite;
            //standard text size
            public static float STANDARD_TEXT_SIZE = 18f;
            //standard button color
            public static Color STANDARD_BUTTON_COLOR = Color.Black;
            //standard button text color
            public static Color STANDARD_BUTTON_TEXT_COLOR = Color.AntiqueWhite;
        }

        public static class securityMessages
        {
            public static string SECURITY_STABLE = "Nothing detected";
            public static string SECURITY_UNSTABLE = "Detected something";
            public static string SECURITY_FIRE_NOT_DETECTED = "Stable";
            public static string SECURITY_FIRE_DETECTED = "Fire detected";
            public static string SECURITY_NOTIFICATION_FIRE_TITLE = "Fire detected";
            public static string SECURITY_NOTIFICATION_FIRE_DESCRIPTION = "Fire was detected near the sensor please go to your device";
        }

    }
}