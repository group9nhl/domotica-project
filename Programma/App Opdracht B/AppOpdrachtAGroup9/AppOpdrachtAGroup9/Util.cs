using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Nio.Charset.Spi;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    /// <summary>
    /// utility class used to make life a little easier
    /// @author Jan Julius de Lang
    /// </summary>
    class Util
    {
#region TimePickerThings
        /// <summary>
        /// returns difference in seconds between now and time in parameter
        /// </summary>
        /// <param name="s">time sent</param>
        /// <returns>seconds between now and s</returns>
        public static long GetTimeDifferenceFrom(long s)
        {
            if (s - GetCurrentTimeInSeconds() < 0)
            {
                return Math.Abs(s - GetCurrentTimeInSeconds());
            }

            return s - GetCurrentTimeInSeconds();
        }

        /// <summary>
        /// returns time in seconds between 2 specified variables
        /// </summary>
        /// <param name="s">var 1</param>
        /// <param name="t">var 2</param>
        /// <returns>time difference between s and t where t is lowest</returns>
        public static long GetTimeDifferenceFrom(long s, long t)
        {
            if (s - t < 0)
            {
                return Math.Abs(s - t);
            }
            return s - t;
        }

        /// <summary>
        /// returns regular time (NOW)
        /// </summary>
        /// <returns></returns>
        public static long GetCurrentTimeInSeconds()
        {
            string[] arr = DateTime.Now.ToString("t").Split(':').ToArray();
            var hrsInSeconds = (Convert.ToInt64(arr[0]) * 60) * 60;
            var minsInSeconds = (Convert.ToInt64(arr[1]) * 60);

            Debug.WriteLine("hrsInSeconds: {0} and minsInSeconds: {1} combined: {2} attempt to normal: {3}:{4}",
                hrsInSeconds, minsInSeconds, hrsInSeconds + minsInSeconds, (hrsInSeconds / 60) / 60, minsInSeconds / 60);

            return hrsInSeconds + minsInSeconds;
        }

        /// <summary>
        /// returns time in seconds based on hour and minute variables
        /// </summary>
        /// <param name="hour">amount of hours e.g 10:15 = (10):15</param>
        /// <param name="minute">amount of minutes e.g 10:15 = 10:(15)</param>
        /// <returns></returns>
        public static long GetCurrentTimeInSeconds(int hour, int minute)
        {
            var hrsInSeconds = (Convert.ToInt64(hour) * 60) * 60;
            var minsInSeconds = (Convert.ToInt64(minute) * 60);

            Debug.WriteLine("hrsInSeconds: {0} and minsInSeconds: {1} combined: {2} attempt to normal: {3}:{4}",
                hrsInSeconds, minsInSeconds, hrsInSeconds + minsInSeconds, (hrsInSeconds / 60) / 60, minsInSeconds / 60);

            return hrsInSeconds + minsInSeconds;
        }

        #endregion

#region sensorValueLimiting
        /// <summary>
        /// limits the sensor value between 0 and 100
        /// </summary>
        /// <param name="v">limits this variable</param>
        /// <returns>variable v after checks</returns>
        public static int LimitSensorValue(int v)
        {
            if (v < 0) return 0;

            if (v > 100) return 100;

            return v;
        }

        /// <summary>
        /// limits the sensor value between given variables
        /// </summary>
        /// <param name="v">value</param>
        /// <param name="max">maximum the value should be able to reach</param>
        /// <param name="min">minimum the value should be able to reach</param>
        /// <returns>variable v after checks</returns>
        public static int LimitSensorValue(int v, int max, int min)
        {
            if (v < min) return min;

            if (v > max) return max;

            return v;
        }

#endregion

        /// <summary>
        /// returns string of binary value given
        /// </summary>
        /// <param name="b">long value</param>
        /// <returns>binary value of b</returns>
        public static string ConvertToBinary(long b)
        {
            return Convert.ToString(b, 2);
        }

#region prettyTextThings
        /// <summary>
        /// sets text components text to given string
        /// </summary>
        /// <param name="textComponent">textview to change</param>
        /// <param name="setTo">string to change to</param>
        public static void setTextComponent(TextView textComponent, string setTo)
        {
            textComponent.Text = setTo;
        }

        /// <summary>
        /// sets the text of a button 
        /// </summary>
        /// <param name="button">button</param>
        /// <param name="setTo">string to change the text to</param>
        public static void setTextComponent(Button button, string setTo)
        {
            button.Text = setTo;
        }

        /// <summary>
        /// set the color of text
        /// </summary>
        /// <param name="t">textview component</param>
        /// <param name="c">color to change to </param>
        public static void setTextColor(TextView t, Color c)
        {
            t.SetTextColor(c);
            t.SetShadowLayer(2f,2f,2f, Color.Rgb(60,55,55));
        }

        /// <summary>
        /// sets the shadow of the text with default values (radius of 2, x of 2, y of 2)
        /// </summary>
        /// <param name="t">textview component</param>
        /// <param name="c"></param>
        public static void setTextShadow(TextView t, Color c)
        {
            t.SetShadowLayer(2f, 2f, 2f, c);
        }

        /// <summary>
        /// sets the text color and shadow of a textview component
        /// </summary>
        /// <param name="t">Textview component to adjust</param>
        /// <param name="textColor">Color of the text</param>
        /// <param name="radius">Radius of the shadow</param>
        /// <param name="dx">Shadow x axis</param>
        /// <param name="dy">Shadow y axis</param>
        /// <param name="shadowColor">Color of the shadow</param>
        public static void setTextColorAndShadow(TextView t, Color textColor, float radius, float dx, float dy, Color shadowColor)
        {
            t.SetTextColor(textColor);
            t.SetShadowLayer(radius, dx, dy, shadowColor);
        }

        /// <summary>
        /// sets the textcolor and shadow the text inside a button component
        /// </summary>
        /// <param name="t">Button component to adjust</param>
        /// <param name="textColor">Color of the text</param>
        /// <param name="radius">Radius of the text shadow</param>
        /// <param name="dx">Shadow on x axis</param>
        /// <param name="dy">Shadow on y axis</param>
        /// <param name="shadowColor">Color of the shadow</param>
        public static void setTextColorAndShadow(Button t, Color textColor, float radius, float dx, float dy, Color shadowColor)
        {
            t.SetTextColor(textColor);
            t.SetShadowLayer(radius, dx, dy, shadowColor);
        }
        #endregion


        /// <summary>
        /// returns the integer stored in char array (for sending data to arduino)
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static char[] getValueInCharArray(int a)
        {
            return a.ToString().ToCharArray();
        }

        /// <summary>
        /// returns the length of an integere in a character array
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static char[] getLengthAsCharArray(int a)
        {
            return a.ToString().Length.ToString().ToCharArray();
        }

#region lazycode
        /// <summary>
        /// returns the length of string as char array
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static char[] getValueInCharArray(string a)
        {
            return a.ToCharArray();
        }

        /// <summary>
        /// returns the value of edittext (mainly jsut being lazy so i dont have to type .text after textview :D)
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static char[] getValueInCharArray(EditText a)
        {
            return a.Text.ToCharArray();
        }

        /// <summary>
        /// returns the length of an integere in a character array
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static char[] getLengthAsCharArray(string a)
        {
            return a.Length.ToString().ToCharArray();
        }

        public static char[] getLengthAsCharArray(TextView a)
        {
            return a.Text.Length.ToString().ToCharArray();
        }
        #endregion
        
            
    }
}