using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "TempratureTabActivity")]
    public class TempratureTabActivity : Activity
    {
        //reference to the maintabactivity so it can be used in this class
        private MainTabActivity mainTabActivity;

        public Button Buttonsubmittimer, Buttonsubmittemp;
            
        public static Button ButtonONOFF;

        public TextView TextTextuseless, TextViewCurrentTemp;

        public EditText editTextTriggerTemp;

        public TimePicker TempratureTimePicker;

        List<Button> TemprButtonlist = new List<Button>();
        List<TextView> TemprTextViewList = new List<TextView>();
        List<EditText> TemprEditTextList = new List<EditText>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Tempr);  

            //sets itself in the connection class so that other classes can find this instance
            Connection.main.TempratureTab = this;

            #region findingComponents

            Buttonsubmittemp = FindViewById<Button>(Resource.Id.button1tempr);
            Buttonsubmittimer = FindViewById<Button>(Resource.Id.button2tempr);
            ButtonONOFF = FindViewById<Button>(Resource.Id.button3tempr);

            TextTextuseless= FindViewById<TextView>(Resource.Id.textView1tempr);
            TextViewCurrentTemp = FindViewById<TextView>(Resource.Id.textView2tempr);

            editTextTriggerTemp = FindViewById<EditText>(Resource.Id.editTextTempr);

            TempratureTimePicker = FindViewById<TimePicker>(Resource.Id.tempTimePicker);
            #endregion

            #region addtolist
            TemprButtonlist.Add(Buttonsubmittemp);
            TemprButtonlist.Add(Buttonsubmittimer);
            TemprButtonlist.Add(ButtonONOFF);
            TemprTextViewList.Add(TextTextuseless);
            TemprTextViewList.Add(TextViewCurrentTemp);
            TemprEditTextList.Add(editTextTriggerTemp);

#endregion

            #region makepretty

            foreach (Button temprbutton in TemprButtonlist)
            {
                Util.setTextColorAndShadow(temprbutton, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            }
            foreach (TextView temprTextView in TemprTextViewList)
            {
                Util.setTextColorAndShadow(temprTextView, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            }
            foreach (EditText temprEditText in TemprEditTextList)
            {
                Util.setTextColorAndShadow(temprEditText, Settings.textInfo.STANDARD_COLOR, 2f, 2f, -2f, Color.Black);
            }
            #endregion

            #region Buttons

            
            
            //button event for switching on and off the temprature kaku
            //checks if button exists
            if (ButtonONOFF != null)
            {
                //on button press
                ButtonONOFF.Click += (sender, e) =>
                {
                    //event
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("w"));
                };
            }

            //button event for sending sensor value limitation to the temprature side of arduino
            //checks if button exists
            if (Buttonsubmittemp != null)
            {
                //on button press
                Buttonsubmittemp.Click += (sender, e) =>
                {
                    //event
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("y"));
                    //using the util class the values are sent to the arduino refer to util class
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes(Util.getLengthAsCharArray(editTextTriggerTemp)));
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes(Util.getValueInCharArray(editTextTriggerTemp)));
                };
            }

            //button event for sending timepicker information to temprature time counting side of arduino
            //checks if button exists
            if (Buttonsubmittimer != null)
            {
                //on button press
                Buttonsubmittimer.Click += (sender, e) =>
                {
                    //event
                    //sets certain values so they are eligble to be sent to arduino
                    char[] timeDifferenceInCharArray =
                        Util.GetTimeDifferenceFrom(Util.GetCurrentTimeInSeconds(TempratureTimePicker.Hour,
                            TempratureTimePicker.Minute)).ToString().ToCharArray();
                    
                    char[] countString = timeDifferenceInCharArray.Length.ToString().ToCharArray();

                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("r"));
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes(countString));
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes(timeDifferenceInCharArray));
                };
            }
            #endregion

            /*
            Switch tempratureSwitch = FindViewById<Switch>(Resource.Id.tempratureSwitch);

            tempratureSwitch.CheckedChange += delegate(object sender, CompoundButton.CheckedChangeEventArgs e)
            {
                Settings.SetKakState(Settings.TEMPRATURE, e.IsChecked);
            };
            */
        }
        
        public void ChangeText(string result, Color color)
        {
            Debug.WriteLine(ButtonONOFF);
            ButtonONOFF.Text = result;
            Util.setTextColor(ButtonONOFF, color);
        }

        public void ChangeTemprature(string result, Color color)
        {
            Debug.WriteLine(TextViewCurrentTemp);
            TextViewCurrentTemp.Text = result;
            Util.setTextColor(TextViewCurrentTemp, color);
        }
    }
}