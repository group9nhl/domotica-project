using System;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Timers;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Text.RegularExpressions;

using Android.Graphics;
using System.Threading.Tasks;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "MainTabActivity")]
    public class MainTabActivity : Activity
    {
        public enum Mode
        {
            Normal,
            Servo,
        };

        public Mode mode = Mode.Normal;

        private TextView infoTextStateKak1, infoTextStateKak2, infoTextStateKak3,
            textViewSensorValue, textViewSensorValue2, bla;
        private Button connectButton;

        private Button kakSwitchButton1, kakSwitchButton2, kakSwitchButton3, buttonServo, buttonSwitchMode; //TODO

        private Button lightButton1;

        private TextView connectedText;
        private TextView uptimeTextView;

        private EditText editTextIPAddress, editTextIPPort;
            
        Timer timerClock, timerSockets;// Timers   
        Socket socket = null;
        List<Tuple<string, TextView>> commandList = new List<Tuple<string, TextView>>();  // List for commands and response places on UI
        int listIndex = 0;

        List<TextView> mainTabTextViews = new List<TextView>();
        List<EditText> mainTabEditTextViews = new List<EditText>();
        List<Button> mainTabButtons = new List<Button>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Main);

            Connection.main.MainTab = this;
            mode = Mode.Normal;

            #region findIds
            connectedText = FindViewById<TextView>(Resource.Id.connectedText);
            uptimeTextView = FindViewById<TextView>(Resource.Id.textUptime);

            connectButton = FindViewById<Button>(Resource.Id.buttonConnect);

            editTextIPAddress = FindViewById<EditText>(Resource.Id.editTextIPAddress);
            editTextIPPort = FindViewById<EditText>(Resource.Id.editTextIPPort);

            buttonServo = FindViewById<Button>(Resource.Id.buttonServo);
            buttonSwitchMode = FindViewById<Button>(Resource.Id.buttonSwitchMode);

            #endregion

            #region addtextviews
            mainTabTextViews.Add(uptimeTextView);
            mainTabTextViews.Add(connectedText);
            #endregion

            #region addEditTexts

            mainTabEditTextViews.Add(editTextIPAddress);
            mainTabEditTextViews.Add(editTextIPPort);

            #endregion

            #region addButtons

            mainTabButtons.Add(connectButton);


            #endregion

            Util.setTextColor(editTextIPAddress, Settings.textInfo.STANDARD_COLOR);
            Util.setTextColor(editTextIPPort, Settings.textInfo.STANDARD_COLOR);

            foreach (Button b in mainTabButtons)
            {
                Util.setTextColor(b, Settings.textInfo.STANDARD_COLOR);
            }

            foreach (var tv in mainTabTextViews)
            {
                Util.setTextColor(tv, Settings.textInfo.STANDARD_COLOR);
            }

            UpdateConnectionState(4, "Disconnected");

            //Debug.WriteLine(Settings.GetMainActivity().test());

            #region addcommandList
            //light status
            commandList.Add(new Tuple<string, TextView>("s", LightTabActivity.lightKakStatus1));
            //temp kak status
            commandList.Add(new Tuple<string, TextView>("x", TempratureTabActivity.ButtonONOFF));
            //temprature value
            commandList.Add(new Tuple<string, TextView>("a", textViewSensorValue));
            //status security
            commandList.Add(new Tuple<string, TextView>("d", textViewSensorValue2));
            //kak status security
            commandList.Add(new Tuple<string, TextView>("z", SecurityTabActivity.buttonONOFFsecurity));
            //fire sensor value
            commandList.Add(new Tuple<string, TextView>("f", bla));
            //lightsensor value ontvangen
            commandList.Add(new Tuple<string, TextView>("l", LightTabActivity.lightSensorDisplayTextView));
            #endregion



            // timer object, running clock
            timerClock = new System.Timers.Timer() { Interval = 2000, Enabled = true }; // Interval >= 1000
            timerClock.Elapsed += (obj, args) =>
            {
                RunOnUiThread(() => { uptimeTextView.Text = DateTime.Now.ToString("h:mm:ss"); });
            };

            // timer object, check Arduino state
            // Only one command can be serviced in an timer tick, schedule from list
            timerSockets = new System.Timers.Timer() { Interval = 1000, Enabled = false }; // Interval >= 750
            timerSockets.Elapsed += (obj, args) =>
            {
                //RunOnUiThread(() =>
                //{
                if (socket != null) // only if socket exists
                {
                    // Send a command to the Arduino server on every tick (loop though list)
                    UpdateGUI(executeCommand(commandList[listIndex].Item1), commandList[listIndex].Item2, listIndex);  //e.g. UpdateGUI(executeCommand("s"), textViewChangePinStateValue);
                    if (++listIndex >= commandList.Count) listIndex = 0;
                }
                else timerSockets.Enabled = false;  // If socket broken -> disable timer
                //});
            };


            if (connectButton != null)  // if button exists
            {
                connectButton.Click += (sender, e) =>
                {
                    //Validate the user input (IP address and port)
                    if (CheckValidIpAddress(editTextIPAddress.Text) && CheckValidPort(editTextIPPort.Text))
                    {
                        ConnectSocket(editTextIPAddress.Text, editTextIPPort.Text);
                        if (socket != null)
                        {
                            if (mode == Mode.Normal)
                            {
                                Connection.main.socket.Send(Encoding.ASCII.GetBytes("g"));
                            }
                            else
                            {
                                Connection.main.socket.Send(Encoding.ASCII.GetBytes("h"));
                            }
                        }
                    }
                    else UpdateConnectionState(3, "Please check IP");
                };
            }
            //textview.Text = "Main tab";
            //SetContentView(textview);

            if (buttonServo != null)  // if button exists
            {
                buttonServo.Click += (sender, e) =>
                {
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("j"));
                };
            }

            if (buttonSwitchMode != null)
            {
                buttonSwitchMode.Click += (sender, e) =>
                {
                    if (mode == Mode.Normal)
                    {
                        Util.setTextComponent(buttonSwitchMode, "Set to Normal");
                        mode = Mode.Servo;
                        Connection.main.socket.Send(Encoding.ASCII.GetBytes("h"));
                    }
                    else if (mode == Mode.Servo)
                    {
                        Util.setTextComponent(buttonSwitchMode, "Set to Servo");
                        mode = Mode.Normal;
                        Connection.main.socket.Send(Encoding.ASCII.GetBytes("g"));
                    }
                };
            }
        }

        //Send command to server and wait for response (blocking)
        //Method should only be called when socket existst
        public string executeCommand(string cmd)
        {
            byte[] buffer = new byte[4]; // response is always 4 bytes
            int bytesRead = 0;
            string result = "---";

            if (socket != null)
            {
                //Send command to server
                socket.Send(Encoding.ASCII.GetBytes(cmd));

                try //Get response from server
                {
                    //Store received bytes (always 4 bytes, ends with \n)
                    bytesRead = socket.Receive(buffer);  // If no data is available for reading, the Receive method will block until data is available,
                    //Read available bytes.              // socket.Available gets the amount of data that has been received from the network and is available to be read
                    while (socket.Available > 0) bytesRead = socket.Receive(buffer);
                    if (bytesRead == 4)
                        result = Encoding.ASCII.GetString(buffer, 0, bytesRead - 1); // skip \n
                    else result = "err";
                }
                catch (Exception exception)
                {
                    result = exception.ToString();
                    if (socket != null)
                    {
                        socket.Close();
                        socket = null;
                    }
                    UpdateConnectionState(3, result);
                }
            }
            return result;
        }

        //Update connection state label (GUI).
        public void UpdateConnectionState(int state, string text)
        {
            // connectButton
            string butConText = "Connect";  // default text
            bool butConEnabled = true;      // default state
            Color color = Color.Red;        // default color
            // pinButton
            bool butPinEnabled = false;     // default state 

            //Set "Connect" button label according to connection state.
            if (state == 1)
            {
                butConText = "Please wait";
                color = Color.Orange;
                butConEnabled = false;
            }
            else
            if (state == 2)
            {
                butConText = "Disconnect";
                color = Color.Green;
                butPinEnabled = true;
            }
            //Edit the control's properties on the UI thread
            RunOnUiThread(() =>
            {

                connectedText.Text = text;
                if (butConText != null)  // text existst
                {
                    connectButton.Text = butConText;
                    connectedText.SetTextColor(color);
                    connectButton.Enabled = butConEnabled;
                }
                //buttonChangePinState.Enabled = butPinEnabled;
                //buttonChangePinState2.Enabled = butPinEnabled;
                //buttonChangePinState3.Enabled = butPinEnabled;
                
            });
        }

        //Update GUI based on Arduino response
        public void UpdateGUI(string result, TextView textview, int id)
        {
            RunOnUiThread(() =>
            {
                Color color = Color.Red;

                if (result == "OFF") color = Color.Red;
                else if (result == " ON") color = Color.Green;
                else color = Settings.textInfo.STANDARD_COLOR;

                Debug.WriteLine("Updating GUI {0} with {1}", id, result);

                switch (id)
                {
                    case 0:
                        if (Connection.main.LightTab != null) Connection.main.LightTab.ChangeText(result, color);
                        break;
                    case 1:
                        if (Connection.main.TempratureTab != null)
                            Connection.main.TempratureTab.ChangeText(result, color);
                        break;
                    case 2:
                        if (Connection.main.TempratureTab != null)
                            Connection.main.TempratureTab.ChangeTemprature(result, color);
                        break;
                    case 3:
                        if(Connection.main.SecurityTab != null)
                        Connection.main.SecurityTab.ChangeSecurityText(result, color);
                        break;
                    case 4:
                        if(Connection.main.SecurityTab != null)
                        Connection.main.SecurityTab.ChangeText(result, color, 2);
                        break;
                    case 5:
                        if (Connection.main.SecurityTab != null)
                        {
                            if(result == "002")
                            {
                                Connection.main.SecurityTab.ChangeFireSensorText(
                                    Settings.securityMessages.SECURITY_FIRE_DETECTED, color);
                                Connection.main.SecurityTab.PushNotification(
                                    Settings.securityMessages.SECURITY_NOTIFICATION_FIRE_TITLE,
                                    Settings.securityMessages.SECURITY_NOTIFICATION_FIRE_DESCRIPTION);
                            }
                            else
                            {
                                Connection.main.SecurityTab.ChangeFireSensorText(
                                    Settings.securityMessages.SECURITY_FIRE_NOT_DETECTED, color);
                            }
                        }
                        break;
                    case 6:
                        if (Connection.main.LightTab != null)
                        {
                            Connection.main.LightTab.changeLightSensorValueText(result, color);
                        }
                        break;
                    default:
                        if (textview != null)
                        {
                            textview.SetTextColor(Settings.textInfo.STANDARD_COLOR);
                            textview.Text = result;
                        }
                        break;
                }
            });
        }

        // Connect to socket ip/prt (simple sockets)
        public void ConnectSocket(string ip, string prt)
        {
            RunOnUiThread(() =>
            {
                if (socket == null)                                       // create new socket
                {
                    UpdateConnectionState(1, "Connecting...");
                    try  // to connect to the server (Arduino).
                    {
                        socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        socket.Connect(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(prt)));
                        if (socket.Connected)
                        {
                            UpdateConnectionState(2, "Connected");
                            timerSockets.Enabled = true;                //Activate timer for communication with Arduino     
                        }
                    }
                    catch (Exception exception)
                    {
                        timerSockets.Enabled = false;
                        if (socket != null)
                        {
                            socket.Close();
                            socket = null;
                        }
                        UpdateConnectionState(4, exception.Message);
                    }
                    Connection.main.socket = socket;
                }
                else // disconnect socket
                {
                    socket.Close(); socket = null;
                    Connection.main.socket = null;
                    timerSockets.Enabled = false;
                    UpdateConnectionState(4, "Disconnected");
                }
            });
        }

        //Close the connection (stop the threads) if the application stops.
        protected override void OnStop()
        {
            base.OnStop();
        }

        //Close the connection (stop the threads) if the application is destroyed.
        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        //Prepare the Screen's standard options menu to be displayed.
        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            //Prevent menu items from being duplicated.
            menu.Clear();

            MenuInflater.Inflate(Resource.Menu.menu, menu);
            return base.OnPrepareOptionsMenu(menu);
        }

        //Executes an action when a menu button is pressed.
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.exit:
                    //Force quit the application.
                    System.Environment.Exit(0);
                    return true;
                case Resource.Id.abort:
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        //Check if the entered IP address is valid.
        private bool CheckValidIpAddress(string ip)
        {
            if (ip != "")
            {
                //Check user input against regex (check if IP address is not empty).
                Regex regex = new Regex("\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|$)){4}\\b");
                Match match = regex.Match(ip);
                return match.Success;
            }
            else return false;
        }

        //Check if the entered port is valid.
        private bool CheckValidPort(string port)
        {
            //Check if a value is entered.
            if (port != "")
            {
                Regex regex = new Regex("[0-9]+");
                Match match = regex.Match(port);

                if (match.Success)
                {
                    int portAsInteger = Int32.Parse(port);
                    //Check if port is in range.
                    return ((portAsInteger >= 0) && (portAsInteger <= 65535));
                }
                else return false;
            }
            else return false;
        }
    }

}