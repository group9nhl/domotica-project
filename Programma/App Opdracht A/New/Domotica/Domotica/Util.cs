using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Debug = System.Diagnostics.Debug;

namespace Domotica
{
    class Util
    {
        public static long GetTimeDifferenceFrom(long s)
        {
            if (s - GetCurrentTimeInSeconds() < 0)
            {
                return Math.Abs(s - GetCurrentTimeInSeconds());
            }

            return s - GetCurrentTimeInSeconds();
        }

        public static long GetTimeDifferenceFrom(long s, long t)
        {
            if (s - t < 0)
            {
                return Math.Abs(s - t);
            }
            return s - t;
        }

        public static string ConvertToBinary(long b)
        {
            return Convert.ToString(b, 2);
        }

        /// <summary>
        /// returns regular time (NOW)
        /// </summary>
        /// <returns></returns>
        public static long GetCurrentTimeInSeconds()
        {
            string[] arr = DateTime.Now.ToString("t").Split(':').ToArray();
            var hrsInSeconds = (Convert.ToInt64(arr[0])*60)*60;
            var minsInSeconds = (Convert.ToInt64(arr[1])*60);

            Debug.WriteLine("hrsInSeconds: {0} and minsInSeconds: {1} combined: {2} attempt to normal: {3}:{4}",
                hrsInSeconds, minsInSeconds, hrsInSeconds + minsInSeconds, (hrsInSeconds/60)/60, minsInSeconds/60);

            return hrsInSeconds + minsInSeconds;
        }

        public static long GetCurrentTimeInSeconds(int hour, int minute)
        {
            var hrsInSeconds = (Convert.ToInt64(hour) * 60) * 60;
            var minsInSeconds = (Convert.ToInt64(minute) * 60);

            Debug.WriteLine("hrsInSeconds: {0} and minsInSeconds: {1} combined: {2} attempt to normal: {3}:{4}",
                hrsInSeconds, minsInSeconds, hrsInSeconds + minsInSeconds, (hrsInSeconds / 60) / 60, minsInSeconds / 60);

            return hrsInSeconds + minsInSeconds;
        }

        public static int LimitSensorValue(int v)
        {
            if (v < 0) return 0;

            if (v > 100) return 100;

            return v;
        }
    }
}