using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "SecurityTabActivity")]
    public class SecurityTabActivity : Activity
    {

        private Button kakuSwitch3;

        public static TextView state;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Security);

            Connection.main.securityTab = this;

            kakuSwitch3 = FindViewById<Button>(Resource.Id.butKaku3);
            state = FindViewById<TextView>(Resource.Id.textSwitch2);

            Debug.WriteLine("textview2: " + state);

            if (kakuSwitch3 != null)
            {
                //sends the time difference in seconds so arduino can count down.
                kakuSwitch3.Click += (sender, e) =>
                {
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("t"));                 // Send toggle-command to the Arduino
                };

            }

        }
        public void ChangeText(string result, Color color)
        {
            Debug.WriteLine("textview by method: " + state);
            state.Text = result;
            state.SetTextColor(color);
        }
    }
}