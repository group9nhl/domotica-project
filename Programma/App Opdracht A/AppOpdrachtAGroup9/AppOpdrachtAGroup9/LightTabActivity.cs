using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "LightTabActivity")]
    public class LightTabActivity : Activity
    {
        private Button kakuSwitch1;
        private Button submitValue;
        private TimePicker timePicker;
        private long timeDifferenceCountDown = 0;

        public static TextView state;
        public static TextView textValue;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Light);
            Connection.main.lightTab = this;
            kakuSwitch1 = FindViewById<Button>(Resource.Id.butKaku1);
            submitValue = FindViewById<Button>(Resource.Id.SubmitTime);
            timePicker = FindViewById<TimePicker>(Resource.Id.timePicker1);
            state = FindViewById<TextView>(Resource.Id.textSwitch1);
            textValue = FindViewById<TextView>(Resource.Id.SensorValueText2);

            Debug.WriteLine("textview1: " + state);

            if (kakuSwitch1 != null)
            {
                kakuSwitch1.Click += (sender, e) =>
                {
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("m"));                 // Send toggle-command to the Arduino
                };
            }

            if (submitValue != null)
            {
                timePicker.TimeChanged += (sender, e) =>
                {
                    Debug.WriteLine(timePicker.Hour + " and " + timePicker.Minute + "\n" + Util.GetCurrentTimeInSeconds());
                };

                submitValue.Click += (sender, e) =>
                {
                    Debug.WriteLine("STP H: {0} ; STP M: {1}", timePicker.Hour, timePicker.Minute);
                    Debug.WriteLine(Util.GetCurrentTimeInSeconds(timePicker.Hour, timePicker.Minute) - Util.GetCurrentTimeInSeconds());
                    timeDifferenceCountDown = Util.GetTimeDifferenceFrom(Util.GetCurrentTimeInSeconds(timePicker.Hour, timePicker.Minute));

                    if (timeDifferenceCountDown > 0)
                    {
                        char[] timeDifferenceInCharArray = timeDifferenceCountDown.ToString().ToCharArray();

                        foreach (var c in timeDifferenceInCharArray)
                        {
                            Debug.WriteLine("TimeDifferenceInCharArray: {0}", c);
                        }

                        char[] countString = timeDifferenceInCharArray.Length.ToString().ToCharArray();

                        foreach (var c in countString)
                        {
                            Debug.WriteLine("countString: {0}", c);
                        }

                        Connection.main.socket.Send(Encoding.ASCII.GetBytes("u"));
                        Connection.main.socket.Send(Encoding.ASCII.GetBytes(countString)); //tostr
                        Connection.main.socket.Send(Encoding.ASCII.GetBytes(timeDifferenceInCharArray)); //tostr
                    }
                };
            }
        }

        public void ChangeText(string result, Color color)
        {
            Debug.WriteLine("textview by method: " + state);
            state.Text = result;
            state.SetTextColor(color);
        }

        public void ChangeValue(string result, Color color)
        {
            textValue.Text = result;
            textValue.SetTextColor(color);
        }
    }
}