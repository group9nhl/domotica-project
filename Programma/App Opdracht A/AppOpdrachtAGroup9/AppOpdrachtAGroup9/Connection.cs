using System;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Timers;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppOpdrachtAGroup9
{
    public class Connection
    {
        public static Connection main;

        public MainTabActivity mainTab;
        public LightTabActivity lightTab;
        public TempratureTabActivity tempTab;
        public SecurityTabActivity securityTab;

        public Socket socket = null;

        static Connection()
        {
            main = new Connection();
        }
    }
}