﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Icu.Text;
using Android.Widget;
using Android.OS;
using Android.Views;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "thomas kys", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : TabActivity
    {
        private List<TabHost.TabSpec> tabList = new List<TabHost.TabSpec>();

        protected override void OnCreate(Bundle bundle)
        {
            //hides the title bar
            RequestWindowFeature(WindowFeatures.NoTitle);


            base.OnCreate(bundle);
            
            SetContentView(Resource.Layout.Main);
            

            //creates the tabs of the application
            CreateTab(typeof(MainTabActivity), "Main_tab", "", Resource.Drawable.ic_tab_main);
            CreateTab(typeof(LightTabActivity), "Light_tab", "", Resource.Drawable.ic_tab_light);
            CreateTab(typeof(TempratureTabActivity), "Temp_tab", "", Resource.Drawable.ic_tab_tempr);
            CreateTab(typeof(SecurityTabActivity), "Sec_tab", "", Resource.Drawable.ic_tab_security);
            

            
        }

        /// <summary>
        /// creates a window tab and uses parameters to style it and give it an id
        /// </summary>
        /// <param name="activityType">type of the tab based on a class all activity classes end on "activity"</param>
        /// <param name="tag">tag of the tab</param>
        /// <param name="label">title for the tab (put nothing to display the image</param>
        /// <param name="drawableId">drawable id resource.drawable</param>
        private void CreateTab(Type activityType, string tag, string label, int drawableId)
        {
            //debug
            Debug.WriteLine("Creating tab: \n ( \n acitivtyType: {0} \n tag: {1} \n label: {2} \n drawableId: {3} \n ) "
                , activityType
                , tag
                , label
                , drawableId);

            var intent = new Intent(this, activityType);
            intent.AddFlags(ActivityFlags.NewTask);
   

            var spec = TabHost.NewTabSpec(tag);
            var drawableIcon = Resources.GetDrawable(drawableId);
            spec.SetIndicator(label, drawableIcon);
            spec.SetContent(intent);

            TabHost.AddTab(spec);
            tabList.Add(spec);
            
        }
    }

}

