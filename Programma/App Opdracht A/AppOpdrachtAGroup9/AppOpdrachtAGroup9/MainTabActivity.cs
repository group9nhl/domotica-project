using System;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Timers;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Text.RegularExpressions;

using Android.Graphics;
using System.Threading.Tasks;
using Debug = System.Diagnostics.Debug;

namespace AppOpdrachtAGroup9
{
    [Activity(Label = "MainTabActivity")]
    public class MainTabActivity : Activity
    {
        private bool connected = false;
        private string connectedAdress = "192.168.1.105";

        private TextView infoTextStateKak1, infoTextStateKak2, infoTextStateKak3,
            textViewSensorValue, textViewSensorValue2;
        private Button connectButton, refreshButton;

        private TextView connectedText;
        private TextView uptimeTextView;

        private EditText editTextIPAddress, editTextIPPort;


        Timer timerClock, timerSockets;// Timers   

        List<TextView> mainTabTextViews = new List<TextView>();
        Socket socket = null;                       // Socket   
        List<Tuple<string, TextView>> commandList = new List<Tuple<string, TextView>>();  // List for commands and response places on UI
        int listIndex = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Main);

            Connection.main.mainTab = this;
            /*
            infoTextStateKak1 = FindViewById<TextView>(Resource.Id.infoTextStateKak1);
            infoTextStateKak2 = FindViewById<TextView>(Resource.Id.infoTextStateKak2);
            infoTextStateKak3 = FindViewById<TextView>(Resource.Id.infoTextStateKak3);
            textViewSensorValue = FindViewById<TextView>(Resource.Id.sensorValueText1);
            textViewSensorValue2 = FindViewById<TextView>(Resource.Id.sensorValueText2);
            */
            connectedText = FindViewById<TextView>(Resource.Id.connectedText);
            uptimeTextView = FindViewById<TextView>(Resource.Id.textUptime);

            connectButton = FindViewById<Button>(Resource.Id.buttonConnect);

            editTextIPAddress = FindViewById<EditText>(Resource.Id.editTextIPAddress);
            editTextIPPort = FindViewById<EditText>(Resource.Id.editTextIPPort);

            #region addtextviews
            mainTabTextViews.Add(uptimeTextView);
            mainTabTextViews.Add(textViewSensorValue2);
            mainTabTextViews.Add(textViewSensorValue);
            mainTabTextViews.Add(infoTextStateKak3);
            mainTabTextViews.Add(infoTextStateKak2);
            mainTabTextViews.Add(infoTextStateKak1);
            mainTabTextViews.Add(connectedText);
            #endregion

            Util.setTextColor(editTextIPAddress, Color.Black);

            UpdateConnectionState(4, "Disconnected");

            //Debug.WriteLine(Settings.GetMainActivity().test());

            // Init commandlist, scheduled by socket timer
            commandList.Add(new Tuple<string, TextView>("s", TempratureTabActivity.state));
            commandList.Add(new Tuple<string, TextView>("x", SecurityTabActivity.state));
            commandList.Add(new Tuple<string, TextView>("z", LightTabActivity.state));
            commandList.Add(new Tuple<string, TextView>("a", TempratureTabActivity.textValue));
            commandList.Add(new Tuple<string, TextView>("b", LightTabActivity.textValue));

            // timer object, running clock
            timerClock = new System.Timers.Timer() { Interval = 2000, Enabled = true }; // Interval >= 1000
            timerClock.Elapsed += (obj, args) =>
            {
                RunOnUiThread(() => { uptimeTextView.Text = DateTime.Now.ToString("h:mm:ss"); });
            };

            // timer object, check Arduino state
            // Only one command can be serviced in an timer tick, schedule from list
            timerSockets = new System.Timers.Timer() { Interval = 1000, Enabled = false }; // Interval >= 750
            timerSockets.Elapsed += (obj, args) =>
            {
                //RunOnUiThread(() =>
                //{
                if (socket != null) // only if socket exists
                {
                    // Send a command to the Arduino server on every tick (loop though list)
                    UpdateGUI(executeCommand(commandList[listIndex].Item1), commandList[listIndex].Item2, listIndex);  //e.g. UpdateGUI(executeCommand("s"), textViewChangePinStateValue);
                    if (++listIndex >= commandList.Count) listIndex = 0;
                }
                else timerSockets.Enabled = false;  // If socket broken -> disable timer
                //});
            };



            //connect button
            connectButton.Click += (sender, e) =>
            {
                //Validate the user input (IP address and port)
                if (CheckValidIpAddress(editTextIPAddress.Text) && CheckValidPort(editTextIPPort.Text))
                {
                    ConnectSocket(editTextIPAddress.Text, editTextIPPort.Text);
                }
                else UpdateConnectionState(3, "Please check IP");
            };

            //textview.Text = "Main tab";
            //SetContentView(textview);
        }

        //Send command to server and wait for response (blocking)
        //Method should only be called when socket existst
        public string executeCommand(string cmd)
        {
            byte[] buffer = new byte[4]; // response is always 4 bytes
            int bytesRead = 0;
            string result = "---";

            if (socket != null)
            {
                //Send command to server
                socket.Send(Encoding.ASCII.GetBytes(cmd));

                try //Get response from server
                {
                    //Store received bytes (always 4 bytes, ends with \n)
                    bytesRead = socket.Receive(buffer);  // If no data is available for reading, the Receive method will block until data is available,
                    //Read available bytes.              // socket.Available gets the amount of data that has been received from the network and is available to be read
                    while (socket.Available > 0) bytesRead = socket.Receive(buffer);
                    if (bytesRead == 4)
                        result = Encoding.ASCII.GetString(buffer, 0, bytesRead - 1); // skip \n
                    else result = "err";
                }
                catch (Exception exception)
                {
                    result = exception.ToString();
                    if (socket != null)
                    {
                        socket.Close();
                        socket = null;
                    }
                    UpdateConnectionState(3, result);
                }
            }
            return result;
        }

        //Update connection state label (GUI).
        public void UpdateConnectionState(int state, string text)
        {
            // connectButton
            string butConText = "Connect";  // default text
            bool butConEnabled = true;      // default state
            Color color = Color.Red;        // default color
            // pinButton
            bool butPinEnabled = false;     // default state 

            //Set "Connect" button label according to connection state.
            if (state == 1)
            {
                butConText = "Please wait";
                color = Color.Orange;
                butConEnabled = false;
            }
            else
            if (state == 2)
            {
                butConText = "Disconnect";
                color = Color.Green;
                butPinEnabled = true;
            }
            //Edit the control's properties on the UI thread
            RunOnUiThread(() =>
            {

                connectedText.Text = text;
                if (butConText != null)  // text existst
                {
                    connectButton.Text = butConText;
                    connectedText.SetTextColor(color);
                    connectButton.Enabled = butConEnabled;
                }
                //buttonChangePinState.Enabled = butPinEnabled;
                //buttonChangePinState2.Enabled = butPinEnabled;
                //buttonChangePinState3.Enabled = butPinEnabled;

            });
        }

        //Update GUI based on Arduino response
        public void UpdateGUI(string result, TextView textview, int id)
        {
            RunOnUiThread(() =>
            {

                Color color = Color.Red;

                if (result == "OFF") color = Color.Red;
                else if (result == " ON") color = Color.Green;


                switch (id)
                {
                    case 0:
                        if (Connection.main.securityTab != null)
                            Connection.main.securityTab.ChangeText(result, color);
                        break;
                    case 1:
                        if (Connection.main.tempTab != null)
                            Connection.main.tempTab.ChangeText(result, color);
                        break;
                    case 2:
                        if (Connection.main.lightTab != null)
                            Connection.main.lightTab.ChangeText(result, color);
                        break;
                    case 3:
                        if (Connection.main.tempTab != null)
                            Connection.main.tempTab.ChangeValue(result, Color.Black);
                        break;
                    case 4:
                        if (Connection.main.lightTab != null)
                            Connection.main.lightTab.ChangeValue(result, Color.Black);
                        break;
                    default:
                        if (textview != null)
                        {
                            textview.SetTextColor(Color.White);
                            textview.Text = result;
                        }
                        break;
                }
            });
        }

        // Connect to socket ip/prt (simple sockets)
        public void ConnectSocket(string ip, string prt)
        {
            RunOnUiThread(() =>
            {
                if (socket == null)                                       // create new socket
                {
                    UpdateConnectionState(1, "Connecting...");
                    try  // to connect to the server (Arduino).
                    {
                        socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        socket.Connect(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(prt)));
                        if (socket.Connected)
                        {
                            UpdateConnectionState(2, "Connected");
                            timerSockets.Enabled = true;                //Activate timer for communication with Arduino     
                        }
                    }
                    catch (Exception exception)
                    {
                        timerSockets.Enabled = false;
                        if (socket != null)
                        {
                            socket.Close();
                            socket = null;
                        }
                        UpdateConnectionState(4, exception.Message);
                    }
                    Connection.main.socket = socket;
                }
                else // disconnect socket
                {
                    socket.Close();
                    socket = null;
                    Connection.main.socket = socket;
                    timerSockets.Enabled = false;
                    UpdateConnectionState(4, "Disconnected");
                }
            });
        }

        //Close the connection (stop the threads) if the application stops.
        protected override void OnStop()
        {
            base.OnStop();
        }

        //Close the connection (stop the threads) if the application is destroyed.
        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
        /*
        //Prepare the Screen's standard options menu to be displayed.
        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            //Prevent menu items from being duplicated.
            menu.Clear();

            MenuInflater.Inflate(Resource.Menu.menu, menu);
            return base.OnPrepareOptionsMenu(menu);
        }

        //Executes an action when a menu button is pressed.
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.exit:
                    //Force quit the application.
                    System.Environment.Exit(0);
                    return true;
                case Resource.Id.abort:
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }*/

        //Check if the entered IP address is valid.
        private bool CheckValidIpAddress(string ip)
        {
            if (ip != "")
            {
                //Check user input against regex (check if IP address is not empty).
                Regex regex = new Regex("\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|$)){4}\\b");
                Match match = regex.Match(ip);
                return match.Success;
            }
            else return false;
        }

        //Check if the entered port is valid.
        private bool CheckValidPort(string port)
        {
            //Check if a value is entered.
            if (port != "")
            {
                Regex regex = new Regex("[0-9]+");
                Match match = regex.Match(port);

                if (match.Success)
                {
                    int portAsInteger = Int32.Parse(port);
                    //Check if port is in range.
                    return ((portAsInteger >= 0) && (portAsInteger <= 65535));
                }
                else return false;
            }
            else return false;
        }
    }

}