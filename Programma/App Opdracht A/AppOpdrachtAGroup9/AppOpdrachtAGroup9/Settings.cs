using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Icu.Text;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AppOpdrachtAGroup9
{
    public class Settings
    {
        private static MainActivity mainactivity;

        public static float STANDARD_TEXT_SIZE = 18f;

        //states of the kaks
        public static bool[] KAKSTATE = new bool[4] { false, false, false, false};
        
        //easy stuff!
        public static int wesket = 0, LIGHT1 = 1, LIGHT2 = 2, TEMPRATURE = 3;

        //names for kak switches
        public static string[] kakname = {"wesket", "light", "light", "temprature"};

        //handles time based events
        public static DateTime[] TimeEvenTimes = new DateTime[4];

        public static void SetKakState(int id)
        {
            KAKSTATE[id] = !KAKSTATE[id];
        }

        public static void SetKakState(int id, bool b)
        {
            KAKSTATE[id] = b;
        }

        public static void setMainActivity(MainActivity m)
        {
            mainactivity = m;
        }

        public static MainActivity GetMainActivity()
        {
            return mainactivity;
        }
    }
}