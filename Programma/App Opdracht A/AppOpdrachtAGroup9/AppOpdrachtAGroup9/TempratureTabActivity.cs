﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Debug = System.Diagnostics.Debug;


namespace AppOpdrachtAGroup9
{
    [Activity(Label = "TempratureTabActivity")]
    public class TempratureTabActivity : Activity
    {

        private Button SubmitValue;
        private Button kakuSwitch2;
        private EditText BorderValue;
        private int sensorBorderValueInt;

        public static TextView state;
        public static TextView textValue;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Tab_Tempr);

            Connection.main.tempTab = this;

            SubmitValue = FindViewById<Button>(Resource.Id.SubmitValue);
            kakuSwitch2 = FindViewById<Button>(Resource.Id.butKaku2);
            BorderValue = FindViewById<EditText>(Resource.Id.BorderValue);
            state = FindViewById<TextView>(Resource.Id.textSwitch3);
            textValue = FindViewById<TextView>(Resource.Id.SensorValueText);

            Debug.WriteLine("textview3: " + state);


            if (SubmitValue != null)
            {

                SubmitValue.Click += (sender, e) =>
                {
                    char[] sensorBorderValueCharArray = BorderValue.Text.ToCharArray();//( ° ͜ ʖ °) 



                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("k"));
                    Debug.WriteLine(sensorBorderValueCharArray.Length.ToString().ToCharArray());
                    Debug.WriteLine(sensorBorderValueCharArray);
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes(sensorBorderValueCharArray.Length.ToString().ToCharArray()));
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes(sensorBorderValueCharArray));
                };
            }

            if (kakuSwitch2 != null)
            {
                kakuSwitch2.Click += (sender, e) =>
                {
                    Connection.main.socket.Send(Encoding.ASCII.GetBytes("q"));                 // Send toggle-command to the Arduino
                };
            }
        }
        public void ChangeText(string result, Color color)
        {
            Debug.WriteLine("textview by method: " + state);
            state.Text = result;
            state.SetTextColor(color);
        }

        public void ChangeValue(string result, Color color)
        {
            textValue.Text = result;
            textValue.SetTextColor(color);
        }
    }
}