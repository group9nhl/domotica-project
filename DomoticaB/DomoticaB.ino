//#include <util.h>
//#include <Twitter.h>
//#include <EthernetUdp2.h>
#include <EthernetServer.h>
#include <EthernetClient.h>
//#include <Dns.h>
//#include <Dhcp.h>
#include <NewRemoteReceiver.h>
#include <NewRemoteTransmitter.h>
#include <secTimer.h>
#include <Servo.h>

// Arduino Domotica server with Klik-Aan-Klik-Uit-controller 
//
// By Sibbele Oosterhaven, Computer Science NHL, Leeuwarden 
// V1.2, 16/12/2016, published on BB. Works with Xamarin (App: Domotica)
//
// Hardware: Arduino Uno, Ethernet shield W5100; RF transmitter on RFpin; debug LED for serverconnection on ledPin
// The Ethernet shield uses pin 10, 11, 12 and 13
// Use Ethernet2.h libary with the (new) Ethernet board, model 2
// IP address of server is based on DHCP. No fallback to static IP; use a wireless router
// Arduino server and smartphone should be in the same network segment (192.168.1.x)
// 
// Supported kaku-devices
// https://eeo.tweakblogs.net/blog/11058/action-klik-aan-klik-uit-modulen (model left)
// kaku Action device, old model (with dipswitches); system code = 31, device = 'A' 
// system code = 31, device = 'A' true/false
// system code = 31, device = 'B' true/false
//
// // https://eeo.tweakblogs.net/blog/11058/action-klik-aan-klik-uit-modulen (model right)
// Based on https://github.com/evothings/evothings-examples/blob/master/resources/arduino/arduinoethernet/arduinoethernet.ino.
// kaku, Action, new model, codes based on Arduino -> Voorbeelden -> RCsw-2-> ReceiveDemo_Simple
//   on      off       
// 1 2210415 2210414   replace with your own codes
// 2 2210413 2210412
// 3 2210411 2210410
// 4 2210407 2210406
//
// https://github.com/hjgode/homewatch/blob/master/arduino/libraries/NewRemoteSwitch/README.TXT
// kaku, Gamma, APA3, codes based on Arduino -> Voorbeelden -> NewRemoteSwitch -> ShowReceivedCode
// 1 Addr 21177114 unit 0 on/off, period: 270us   replace with your own code
// 2 Addr 21177114 unit 1 on/off, period: 270us
// 3 Addr 21177114 unit 2 on/off, period: 270us

// Supported KaKu devices -> find, download en install corresponding libraries
#define unitCodeApa3      22731934  // replace with your own code
#define unitCodeActionOld 31        // replace with your own code
#define unitCodeActionNew 2210406   // replace with your own code

// Include files.
#include <SPI.h>                  // Ethernet shield uses SPI-interface
#include <Ethernet2.h>             // Ethernet library (use Ethernet2.h for new ethernet shield v2)
#include <NewRemoteTransmitter.h> // Remote Control, Gamma, APA3
//#include <RCSwitch.h>           // Remote Control, Action, new model

// Set Ethernet Shield MAC address  (check yours)
byte mac[] = { 0x46, 0x6c, 0x8f, 0x36, 0x84, 0x8a }; // Ethernet adapter shield S. Oosterhaven
int ethPort = 3300;                                  // Take a free port (check your router)

#define RFPin        7  // output, pin to control the RF-sender (and Click-On Click-Off-device)
#define lowPin       5  // output, always LOW
#define ledPin       8 // output, led used for "connect state": blinking = searching; continuously = connected
#define infoPin      9  // output, more information
#define analogPin    0  // sensor 1 value
#define analogPin    1 // sensor 2 value
#define analogPin    2
#define trigPin     2
#define echoPin     3
#define buzzer      4
#define servoPin    6

Servo servo1;

EthernetServer server(ethPort);              // EthernetServer instance (listening on port <ethPort>).
NewRemoteTransmitter apa3Transmitter(unitCodeApa3, RFPin, 260, 3);  // APA3 (Gamma) remote, use pin <RFPin> 
//RCSwitch mySwitch = RCSwitch();            // Remote Control, Action, new model (on-off), use pin <RFPin>

char actionDevice = 'A';                 // Variable to store Action Device id ('A', 'B', 'C')
secTimer myTimer; //lightsensor
secTimer myTimerTemp; //temprature sensor
long seconds=0;
long seconds2=0; //for temp
bool pinChange = false;                  // Variable to store actual pin change
int SensorLight = 0;                    // Variable to store actual sensor value
int SensorTemperatuur = 0;
int sensorValueFire = 0;
int sensorValueUltra = 0;
bool collecting = false;
int num = 0;
int count = 0;
int valFire = 0;
char numbers[5];
char colType = '0';
long kakuTimer = 0;
int kakuBorder = 0;
int currentPos = 0;
int duration, distance;


bool kaku1 = false;
bool kaku2 = false;
bool kaku3 = false;

bool securityStatus = false;

int lightSensorBorder;
int lightTimePickerCountDown;
bool lightTimePickerCountDownBool;

int tempSensorBorder;
int tempTimePickerCountDown;
bool tempTimePickerCountDownBool;

bool modeIsServo = true;
bool toggleServo = false;
bool waitForInput = false;
bool waitForInput2 = false;

void setup()
{
   Serial.begin(9600);
   //while (!Serial) { ; }               // Wait for serial port to connect. Needed for Leonardo only.

   servo1.attach(servoPin);
   servo1.write(40);

   Serial.println("Domotica project, Groep 9.\n");
   
   //Init I/O-pins
   pinMode(echoPin, INPUT); 
   pinMode(lowPin, OUTPUT);
   pinMode(RFPin, OUTPUT);
   pinMode(ledPin, OUTPUT);
   pinMode(infoPin, OUTPUT);
   pinMode(trigPin,OUTPUT);
   pinMode(buzzer, OUTPUT);
   
   //Default states
   digitalWrite(lowPin, LOW);
   digitalWrite(RFPin, LOW);
   digitalWrite(ledPin, LOW);
   digitalWrite(infoPin, LOW);
   

   //Try to get an IP address from the DHCP server.
   if (Ethernet.begin(mac) == 0)
   {
      Serial.println("Servo mode engaged");
      while (true){     // no point in carrying on, so do nothing forevermore; check your router

        int duration;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  int distance = (duration / 2)* 0.0344;

      delay(200);
      Serial.println(distance);
      if(distance > 1 && distance <= 7)
          {
            servo1.write( 140 );
          }else{
            servo1.write( 40 );
          }
        
      }
   }
   
   Serial.print("LED (for connect-state and pin-state) on pin "); Serial.println(ledPin);
   Serial.println("Ethernetboard connected (pins 10, 11, 12, 13 and SPI)");
   Serial.println("Connect to DHCP source in local network (blinking led -> waiting for connection)");
   
   //Start the ethernet server.
   server.begin();

   // Print IP-address and led indication of server state
   Serial.print("Listening address: ");
   Serial.print(Ethernet.localIP());
   
   // for hardware debug: LED indication of server state: blinking = waiting for connection
   int IPnr = getIPComputerNumber(Ethernet.localIP());   // Get computernumber in local network 192.168.1.3 -> 3)
   Serial.print(" ["); Serial.print(IPnr); Serial.print("] "); 
   Serial.print("  [Testcase: telnet "); Serial.print(Ethernet.localIP()); Serial.print(" "); Serial.print(ethPort); Serial.println("]");
   signalNumber(ledPin, IPnr);
}

void loop()
{

   // Listen for incomming connection (app)
   EthernetClient ethernetClient = server.available();
   if (!ethernetClient) {
      blink(ledPin);
      return; // wait for connection and blink LED
   }

   Serial.println("Application connected");
   digitalWrite(ledPin, LOW);

   // Do what needs to be done while the socket is connected.
   while (ethernetClient.connected()) 
   {

      if(!modeIsServo)
      {
      AlarmUltra();
      }
      else
      {
        int duration;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  int distance = (duration / 2)* 0.0344;

  sensorValueUltra = distance;
        
        if(toggleServo)
        {
          if(!waitForInput)
          {
            Serial.println("lowering servo...");
            // doe de slagboom naar beneden als die nog niet beneden is
              currentPos = 0;
              servo1.write( 140 );
              waitForInput = true;
              Serial.println("Servo is down");
          }
          if(distance > 1 && distance <= 5  && waitForInput)
          {
              Serial.println("servo is going up...");
            currentPos = 90; // doe de slagboom omhoog als hij nog niet omhoog staat
            servo1.write( 40 );
            waitForInput = false;
            toggleServo = false;
          }
          
        }
      }
      
      
      if(kakuBorder != 0)
      {
        if(SensorTemperatuur > kakuBorder)
        {
              kaku2 = true;
         
              apa3Transmitter.sendUnit(1, kaku2);   
        }else{
              kaku2 = false;
         
              apa3Transmitter.sendUnit(1, kaku2);
        }
      }




      if(lightTimePickerCountDown != 0)
      {
      if (seconds!=myTimer.readTimer())
      {
        seconds=myTimer.readTimer();
      }
      }
      if(seconds > lightTimePickerCountDown)
      {
        kaku1 = true;
        apa3Transmitter.sendUnit(2, kaku1);
        myTimer.stopTimer();
        seconds = 0;
        kakuTimer = 0;
      }

      if(tempTimePickerCountDown != 0)
      {
        if(seconds2!=myTimerTemp.readTimer())
        {
          seconds2=myTimerTemp.readTimer();
        }
      }
      if(seconds2 > tempTimePickerCountDown){
        kaku2 = true;
        apa3Transmitter.sendUnit(2, kaku2);
        myTimerTemp.stopTimer();
        seconds2 = 0;
        tempTimePickerCountDown = 0;
      }
      
      
      SensorLight = readSensor(0, 100);         // update sensor value
      SensorTemperatuur = readSensor(1, 100);    
      sensorValueFire = readSensor(2, 100);

            if (sensorValueFire >= 2)
  {
  tone(buzzer, 1000); // Send 1KHz sound signal...
  delay(1000);        // ...for 1 sec
  noTone(buzzer);     // Stop sound...
  delay(1000);        // ...for 1sec

  apa3Transmitter.sendUnit(0, false);
  apa3Transmitter.sendUnit(1, false);
  apa3Transmitter.sendUnit(2, false);
  }


      
      // Execute when byte is received.
      while (ethernetClient.available())
      {
         char inByte = ethernetClient.read();   // Get byte from the client

         if(collecting){
            if(num == 0){
                num = String(inByte).toInt();
              }else{

              numbers[count] = (int)inByte;
              count++;

              if(count == num)
              {
                collecting = false;
                num = 0;
                count = 0;

                switch(colType)
                {
                  case 'u':
                  lightSensorBorder = String(numbers).toInt();
                  Serial.print("lightSensorBorder: ");
                  Serial.println(lightSensorBorder);
                  break;
                  
                  case 'v':
                  lightTimePickerCountDown = String(numbers).toInt();
                  Serial.print("lightTimePickerCountDown: ");
                  Serial.println(lightTimePickerCountDown);
                  myTimer.startTimer();
                  break;
                  
                  case 'y':
                  tempSensorBorder = String(numbers).toInt();
                  Serial.print("tempSensorBorder: ");
                  Serial.println(tempSensorBorder);
                  break;
                  
                  case 'r':
                  tempTimePickerCountDown = String(numbers).toInt();
                  Serial.print("tempTimePickerCountDown: ");
                  Serial.println(tempTimePickerCountDown);
                  myTimer.startTimer();
                  break;
                  
                  case 'k':
                  kakuBorder = String(numbers).toInt();
                  Serial.print("Border: ");
                  Serial.println(kakuBorder);
                  break;
                }

                
                Serial.print("numbers: ");
                Serial.println(numbers);
              }
              }
         }
         if(!collecting){
         executeCommand(inByte, ethernetClient);                // Wait for command to execute
         //if (inByte != 'z')Serial.println(inByte);

         char inByte = ethernetClient.read();   // Get byte from the client.
         executeCommand(inByte, ethernetClient);                // Wait for command to execute
         inByte = NULL;                         // Reset the read byte.
         }
      } 
   }
   Serial.println("Application disonnected");
}



// Do what needs to be done while the socket is connected.
// Implementation of (simple) protocol between app and Arduino
// Request (from app) is single char ('a', 's', 't', 'i' etc.)
// Response (to app) is 4 chars  (not all commands demand a response)

void executeCommand(char cmd,EthernetClient ethernetClient)
{     
         char buf[4] = {'\0', '\0', '\0', '\0'};
         char buf1[4] = {'\0', '\0', '\0', '\0'};
         char buf2[4] = {'\0', '\0', '\0', '\0'};
         
         switch (cmd) {
         case 'l': // Report sensor value to the app  
            intToCharBuf(SensorLight, buf, 4);                // convert to charbuffer
            server.write(buf, 4);                             // response is always 4 chars (\n included)
            break;
        case 'a': // Report sensor value to the app        
            intToCharBuf1(SensorTemperatuur, buf1, 4);                // convert to charbuffer
            server.write(buf1, 4);                             // response is always 4 chars (\n included)
            break;
        case 'f': // Report sensor value to the app
            intToCharBuf2(sensorValueFire, buf2, 4);
            server.write (buf2, 4);
              break;
         case 's': // Report switch state to the app
            if (kaku1) { server.write(" ON\n"); }  // always send 4 chars
            else { server.write("OFF\n");  }
            break;

         case 'x': // Report switch state to the app
            if (kaku2) { server.write(" ON\n"); }  // always send 4 chars
            else { server.write("OFF\n"); }
            break;         
            
          case 'z': // Report switch state to the app
            if (kaku3) { server.write(" ON\n");}  // always send 4 chars
            else { server.write("OFF\n"); }
            break;
            
         case 't': // Toggle state; If state is already ON then turn it OFF
            
         kaku1 = !kaku1;
         
         apa3Transmitter.sendUnit(0, kaku1);

         break;
         
         case 'w' :

         kaku2 = !kaku2;
         
         apa3Transmitter.sendUnit(1, kaku2);

         break;

         case 'e' :

        
         kaku3 = !kaku3;
         
         apa3Transmitter.sendUnit(2, kaku3);
    

         break;

         case 'u':
         collecting = true;
         colType = 'u';
         break;  
         
         case 'v':
         collecting = true;
         colType = 'v';
         break;

         case 'y':  
         collecting = true;
         colType = 'y';
         break;

         case 'r':
         collecting = true;
         colType = 'r';
         break;

         case 'k':
         collecting = true;
         colType = 'k';
         break;

        case 'g':
        Serial.println("set servomode false");
        modeIsServo = false;
        break;

        case 'h':
        Serial.println("set servomode true");
        modeIsServo = true;
        break;
        
       case 'j':
       Serial.println("set the servo down");
        toggleServo = true;
       break;
         
         
         case 'd':
if(securityStatus){ server.write("ON\n"); }
else { server.write("OFF\n"); }
         break;
         
         case 'i':    
            digitalWrite(infoPin, HIGH);
            break;
         default:
            digitalWrite(infoPin, LOW);
         }

}

// read value from pin pn, return value is mapped between 0 and mx-1




int readSensor(int pn, int mx)
{
  return map(analogRead(pn), 0, 1023, 0, mx);    
}

// Convert int <val> char buffer with length <len>
void intToCharBuf(int val, char buf[], int len)
{
   String s;
   s = String(val);                        // convert tot string
   if (s.length() == 1) s = "0" + s;       // prefix redundant "0" 
   if (s.length() == 2) s = "0" + s;  
   s = s + "\n";                           // add newline
   s.toCharArray(buf, len);                // convert string to char-buffer
}
void intToCharBuf1(int val1, char buf1[], int len1)
{
   String f;
   f = String(val1);                        // convert tot string
   if (f.length() == 1) f = "0" + f;       // prefix redundant "0" 
   if (f.length() == 2) f = "0" + f;  
   f = f + "\n";                           // add newline
   f.toCharArray(buf1, len1);                // convert string to char-buffer
}

void intToCharBuf2(int val2, char buf2[], int len2)
{
   String g;
   g = String(val2);                        // convert tot string
   if (g.length() == 1) g = "0" + g;       // prefix redundant "0" 
   if (g.length() == 2) g = "0" + g;  
   g = g + "\n";                           // add newline
   g.toCharArray(buf2, len2);                // convert string to char-buffer
}
// Check switch level and determine if an event has happend
// event: low -> high or high -> low
void checkEvent(int p, bool &state)
{
   static bool swLevel = false;       // Variable to store the switch level (Low or High)
   static bool prevswLevel = false;   // Variable to store the previous switch level

   swLevel = digitalRead(p);
   if (swLevel)
      if (prevswLevel) delay(1);
      else {               
         prevswLevel = true;   // Low -> High transition
         state = true;
         pinChange = true;
      } 
   else // swLevel == Low
      if (!prevswLevel) delay(1);
      else {
         prevswLevel = false;  // High -> Low transition
         state = false;
         pinChange = true;
      }
}

// blink led on pin <pn>
void blink(int pn)
{
  digitalWrite(pn, HIGH); 
  delay(100); 
  digitalWrite(pn, LOW); 
  delay(100);
}

// Visual feedback on pin, based on IP number, used for debug only
// Blink ledpin for a short burst, then blink N times, where N is (related to) IP-number
void signalNumber(int pin, int n)
{
   int i;
   for (i = 0; i < 30; i++)
       { digitalWrite(pin, HIGH); delay(20); digitalWrite(pin, LOW); delay(20); }
   delay(1000);
   for (i = 0; i < n; i++)
       { digitalWrite(pin, HIGH); delay(300); digitalWrite(pin, LOW); delay(300); }
    delay(1000);
}

// Convert IPAddress tot String (e.g. "192.168.1.105")
String IPAddressToString(IPAddress address)
{
    return String(address[0]) + "." + 
           String(address[1]) + "." + 
           String(address[2]) + "." + 
           String(address[3]);
}

// Returns B-class network-id: 192.168.1.3 -> 1)
int getIPClassB(IPAddress address)
{
    return address[2];
}

// Returns computernumber in local network: 192.168.1.3 -> 3)
int getIPComputerNumber(IPAddress address)
{
    return address[3];
}

// Returns computernumber in local network: 192.168.1.105 -> 5)
int getIPComputerNumberOffset(IPAddress address, int offset)
{
    return getIPComputerNumber(address) - offset;
}

void servo (){

  digitalWrite(trigPin, HIGH);
  delay(10);
  digitalWrite(echoPin, LOW);

  duration = pulseIn (echoPin, HIGH);
  distance = (duration / 2) / 29.1;

    if( distance <= 5 )
    {
        if( currentPos < 90 )
        {
            
            currentPos++; // doe de slagboom omhoog als hij nog niet omhoog staat
            servo1.write( currentPos );
        }
    }
    else 
    {
        if( currentPos > 0 )
        {
          // doe de slagboom naar beneden als die nog niet beneden is
            currentPos--;
            servo1.write( currentPos );
        }
    }
}
  
void AlarmUltra()
{
  int duration;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  int distance = (duration / 2)* 0.0344;

  sensorValueUltra = distance;

  //Serial.println(sensorValueUltra);

  if (distance > 10 && distance < 25)
  {
    securityStatus = true;
tone(buzzer, 500); // Send 1KHz sound signal...
  delay(50);        // ...for 1 sec
  noTone(buzzer);     // Stop sound...
  delay(50);        // ...for 1sec
}
}
